#!/bin/bash
docker run --rm --network infrastructure_production_global mongo:latest bash -c " \
    mongo --host kominal/mongodb01,mongodb02,mongodb03 ${STACK_NAME} --authenticationDatabase admin -u admin -p ${DATABASE_ADMIN_PASSWORD} --eval \"db.createUser({ user:'user-service', pwd:'${DATABASE_PASSWORD_USER_SERVICE}', roles:[{ role:'readWrite', db:'${STACK_NAME}_user-service' }] })\"; \
    mongo --host kominal/mongodb01,mongodb02,mongodb03 ${STACK_NAME} --authenticationDatabase admin -u admin -p ${DATABASE_ADMIN_PASSWORD} --eval \"db.createUser({ user:'live-service', pwd:'${DATABASE_PASSWORD_LIVE_SERVICE}', roles:[{ role:'readWrite', db:'${STACK_NAME}_live-service' }] })\"; \
    mongo --host kominal/mongodb01,mongodb02,mongodb03 ${STACK_NAME} --authenticationDatabase admin -u admin -p ${DATABASE_ADMIN_PASSWORD} --eval \"db.createUser({ user:'push-service', pwd:'${DATABASE_PASSWORD_PUSH_SERVICE}', roles:[{ role:'readWrite', db:'${STACK_NAME}_push-service' }] })\"; \
"